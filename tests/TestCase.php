<?php
namespace Rw\SharpspringApi\Tests;

use PHPUnit_Framework_TestCase as PHPUnit;

/**
 * Class TestCase
 */
class TestCase extends PHPUnit
{

    public function __construct()
    {
        parent::__construct();
    }

    public function setUp()
    {
        parent::setUp();
    }

    public function tearDown()
    {
        parent::tearDown();
    }

}
