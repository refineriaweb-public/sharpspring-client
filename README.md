# SharpSpring Client

Paquete de Composer que permite dotar a un proyecto de las herramientas necesarias para interactuar con la API de la plataforma SharpSpring.
* [PHP Doc](https://refineriaweb-public.gitlab.io/sharpspring-client/) (en inglés)
* [Api de SharpSpring](https://wiwink.readme.io/reference) (Para fines documentales)

## Requisitos
* PHP 5.6.0 o superior.
* Extensión CURL de PHP.

## Instalación
Para llevar a cabo la instalación de esta librería en cualquier proyecto que cumpla los requisitos, deben seguirse los pasos que se detallan a continuación:

1. Añadir el repositorio correspondiente como última clave de nuestro archivo `composer.json`.

```json
"repositories": {
    "sharpspring-client": {
        "type": "vcs",
        "url": "https://gitlab.com/refineriaweb-public/sharpspring-client"
    }
}
```

2. Añadir el siguiente elemento dentro de la clave `require`.

```json
"refineriaweb/sharpspring-api": "^0.0.1"
```

3. Ejecutar el comando `composer install`. Si no sucede nada, eliminamos el archivo `composer.lock` y reintentamos.

4. Comprobar que el archivo de configuración `sharpspring.php` se ha creado en el directorio `config/`. Si no, ejecutar el comando `php artisan vendor:publish --provider="Rw\SharpspringApi\Larave\SharpspringServiceProvider" --tag=config --force`. Si no, en última instancia, crearlo manualmente e incluir el siguente contenido:
```php
<?php
return [
    'account_id' => env('SHARPSPRING_ACCOUNT_ID', ''),
    'secret_key' => env('SHARPSPRING_SECRET_KEY', '')
];

```
5. Asegurarnos de que en el `.env` tenemos definida las clave `SHARPSPRING_ACCOUNT_ID` y `SHARPSPRING_SECRET_KEY` con los datos de API proporcionados por SharpSpring como valor.

## Actualización
Para actualizar el paquete en caso necesario, basta con ejecutar el comando `composer update`.

## Uso
La librería está planteada de tal manera que únicamente sea necesario llamar métodos a través de la [facade](https://laravel.com/docs/5.5/facades) correspondiente (Sharpspring::) para ejecutar las acciones necesarias.

Los métodos disponibles son:

### Accounts (Cuentas)
* [Sharpspring@createAccounts(Account[] $accounts) : CreateResult[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_createAccounts)
* [Sharpspring@createAccount(Account $account) : CreateResult](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_createAccount)
* [Sharpspring@deleteAccounts(int[] $ids) : DeleteResult[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_deleteAccounts)
* [Sharpspring@getAccount(int $id) : ? Account](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getAccount)
* [Sharpspring@getAccounts(array $where = [], ? int $limit = null, ? int $offset = null) : Account[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getAccounts)
* [Sharpspring@getAccountsDateRange(string $startDate, string $endDate, string $timestamp) : Account[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getAccountsDateRange)
* [Sharpspring@updateAccounts(Accounts[] $accounts) : UpdateResult[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_updateAccounts)

### Campaigns (Campañas)
* [Sharpspring@createCampaigns(Campaign[] $campaigns) : CreateResult[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_createCampaigns)
* [Sharpspring@deleteCampaigns(int[] $ids) : DeleteResult[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_deleteCampaigns)
* [Sharpspring@getCampaign(int $id) : ? Campaign](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getCampaign)
* [Sharpspring@getCampaigns(array $where = [], ? int $limit = null, ? int $offset = null) : Campaign[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getCampaigns)
* [Sharpspring@getCampaignsDateRange(string $startDate, string $endDate, string $timestamp) : Campaign[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getCampaignsDateRange)
* [Sharpspring@updateCampaigns(Campaigns[] $campaigns) : UpdateResult[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_updateCampaigns)

### Clients (Clientes)
* [Sharpspring@getClients() : Client[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getClients)

### Deal stages
* [Sharpspring@createDealStages(DealStage[] $dealStages) : CreateResult[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_createDealStages)
* [Sharpspring@deleteDealStages(int[] $ids) : DeleteResult[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_deleteDealStages)
* [Sharpspring@getDealStage(int $id) : ? DealStage](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getDealStage)
* [Sharpspring@getDealStages(array $where = [], ? int $limit = null, ? int $offset = null) : DealStage[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getDealStages)
* [Sharpspring@getDealStagesDateRange(string $startDate, string $endDate, string $timestamp) : DealStage[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getDealStagesDateRange)

### Email (Correo)
* [Sharpspring@createEmail(Email[] $emails) : CreateResult[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_createEmail)
* [Sharpspring@deleteEmail(int[] $ids) : DeleteResult[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_deleteEmail)
* [Sharpspring@getEmail(int $id) : ? Email](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getEmail)
* [Sharpspring@getEmailListing(array $where = [], ? int $limit = null, ? int $offset = null) : EmailListing[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getEmailListing)
* [Sharpspring@updateEmail(Emails[] $emails) : UpdateResult[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_updateEmail)

### Fields (Campos)
* [Sharpspring@createFields(Field[] $fields) : CreateResult[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_createFields)
* [Sharpspring@deleteFields(int[] $ids) : DeleteResult[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_deleteFields)
* [Sharpspring@getFields(array $where = [], ? int $limit = null, ? int $offset = null) : Fields[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getFields)
* [Sharpspring@updateFields(Field[] $fields) : UpdateResult[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_updateFields)

### ListTags
* [Sharpspring@getListTags(Field[] $fields) : iterable](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getListTags)

### ListMembers
* [Sharpspring@getListMembers(array $where = [], ? int $limit = null, ? int $offset = null) : iterable](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getListMembers)

### Leads (Contactos)
* [Sharpspring@createLeads(Lead[] $leads) : CreateResult[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_createLeads)
* [Sharpspring@deleteLeads(int[] $ids) : DeleteResult[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_deleteLeads)
* [Sharpspring@getLead(int $id) : ? Lead](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getLead)
* [Sharpspring@getLeads(array $where = [], ? int $limit = null, ? int $offset = null) : Lead[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getLeads)
* [Sharpspring@getLeadsDateRange(string $startDate, string $endDate, string $timestamp) : Lead[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getLeadsDateRange)
* [Sharpspring@suscribeToLeadUpdates(string $url) : bool](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_subscribeToLeadUpdates)
* [Sharpspring@updateLeads(Leads[] $leads) : UpdateResult[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_updateLeads)
* [Sharpspring@updateLeadsV2(Leads[] $leads) : UpdateResult[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_updateLeadsV2)
* [Sharpspring@getLeadByEmail(string $emailAddress) : ? Lead](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getLeadByEmail)
### Lists (Listas)
* [Sharpspring@createList() : void](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_createList) -> \[WIP\]
* [Sharpspring@deleteList(int[] $ids) : DeleteResult[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_deleteList)
* [Sharpspring@getActiveLists(array $where = [], ? int $limit = null, ? int $offset = null) : Lists[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getActiveLists)
* [Sharpspring@activeList(int $id) : ? Lists](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getActiveList)
* [Sharpspring@getListMembers(array $where = [], ? int $limit = null, ? int $offset = null) : ListMember[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getListMembers)
* [Sharpspring@addListMember(int $listID, int $memeberID) : ? CreateResult](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_addListMember)
* [Sharpspring@addListMemberEmailAddress(int $listID, string $emailAddress) : ? CreateResult](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_addListMemberEmailAddress)
* [Sharpspring@addListMembers(int $id) : ? CreateResult[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_addListMembers)
* [Sharpspring@addListMembersEmailAddress(int $id) : ? CreateResult[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_addListMembersEmailAddress)
* [Sharpspring@getContactListMemberships(array $where = []) : ? ListMember[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getContactListMemberships)
### Opportunities (Oportunidades)
* [Sharpspring@createOpportunities(Opportunity[] $opportunities) : CreateResult[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_createOpportunities)
* [Sharpspring@createOpportunity(Opportunity $opportunity) : CreateResult](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_createOpportunity)
* [Sharpspring@deleteOpportunities(int[] $ids) : DeleteResult[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_deleteOpportunities)
* [Sharpspring@getOpportunities(array $where = [], ? int $limit = null, ? int $offset = null) : Opportunity[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getOpportunities)
* [Sharpspring@getOpportunitiesDateRange(string $startDate, string $endDate, string $timestamp) : Opportunity[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getOpportunitiesDateRange)
* [Sharpspring@getOpportunity(int $id) : ? Opportunity](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getOpportunity)
* [Sharpspring@updateOpportunities(Opportunity[] $opportunities) : UpdateResult[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_updateOpportunities)
* [Sharpspring@updateOpportunity(Opportunity $opportunity) : UpdateResult](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_updateOpportunity)
### Oportunity Leads (Leads de oportunidades)
* [Sharpspring@createOpportunityLeads(OpportunityLead[] $opportunityLeads) : CreateResult[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_createOpportunityLeads)
* [Sharpspring@deleteOpportunityLeads(int[] $ids) : DeleteResult[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_deleteOpportunityLeads)
* [Sharpspring@getOpportunityLead(int $id) : OpportunityLead](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getOpportunityLead)
* [Sharpspring@getOpportunityLeads(array $where = [], ? int $limit = null, ? int $offset = null) : OpportunityLead[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getOpportunityLeads)
* [Sharpspring@getOpportunityLeadsDateRange(string $startDate, string $endDate, string $timestamp) : OpportunityLead[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getOpportunityLeadsDateRange)
### User profiles (Perfiles de usuarios de Sharp)
* [Sharpspring@getUserProfiles(array $where = [], ? int $limit = null, ? int $offset = null) : UserProfile[]](https://refineriaweb-public.gitlab.io/sharpspring-client/classes/Rw-SharpspringApi-SharpspringClient.html#method_getUserProfiles)
## Ejemplos
### Creación de Lead
A continuación se muestra un ejemplo de código utilizado para la creación de un lead.
```php
private function exampleCreateLead() : void
{
    $data = [
        'emailAddress' => post('email'),
        'firstName' => post('name'),
        'phoneNumber' => post('telephone'),
        //Campos personalizados
        'quiero_alquilar_____en_qu___podemos_ayudarte___1__612771802e32b' => post('content'),
        'oficina_de_inter__s_605c72862e6ba' => post('office'),
        'quiero_alquilar__1__6127716bb44a9' => true,
        'comunicaci__n_comercial_5fb3a48033b84' => 2,
        'opt_in_5fad09302b084' => true,
        'fecha___quiero_alquilar__1__612771529fde4' => Carbon::now()->toDateTimeString()
    ];
    
    $lead = new Lead($data);

    Sharpspring::createLeads($lead);
}
```
### Actualización de Opportunity
A continuación se muestra un ejemplo de código utilizado para la actualización de una opportunity.
```php
private function exampleUpdateOpportunity(Lead $lead) : void
{
    $now = Carbon::now();
    $agent = Sharpspring::getUserProfiles(['emailAddress' => 'info@refineriaweb.com'])[0];

    $data = [
        'ownerID' => $agent->id,
        'primaryLeadID' => $lead->id,
        'probability' => 0
        'dealStageID' => 502798339,
        'opportunityName' => 'Interesado en alquilar - '
            . $now->toDateTimeString()
            . '.'
            . $now->millisecond,
        'closeDate' => Carbon::now()->toDateString()
        ////Campos personalizados
        'comercial_asignado_6061c15671b84' => $estate->agent->fullName,
        'ref__consultada_6061c18655bc2' => $reference,
        'tipo_de_transacci__n_6061c1b383a8f' => $estate->operation_id === Operation::SELL
            ? 'Compra'
            : 'Alquiler',
        'zonas_6061c1ef23e2b' => $estate->district->name,
        'tipos_6061c2049589c' => $estate->type->name,
        '__en_qu___podemos_ayudarte__6061c2a7b15b0' => $info,
        'ejemplo1_606ebfec35c5f' => null,
    ];
    
    $opportunity = new Opportunity($data);

    Sharpspring::updateOpportunity($opportunity);
}
```
### Get fields
A continuación se muestra un ejemplo de código utilizado para la obtención de todos los campos definidos para esa cuenta de SharpSpring.
```php
Sharpspring::getFields();
``` 
### Ejemplo de un resultado de creación
Cuando hacemos una petición, ya sea de creación, de actualización o de eiliminación, se genera un resultado con una estructura similar a esta (ejemplo de creación):
```json
{
    "result": {
        "creates": [
            {
                "success": true,
                "error": null,
                "id": 770490417155 //Super importante. Se puede recoger para usar en otras operaciones a posteriori.
            }
        ]
    },
    "error": null,
    "id": "", // Este ID es el de la petición, no el del objeto de sharpspring. Normalmente viene vacío y su uso no es habitual.
    "callCount": "1",
    "queryLimit": "50000"
}
```
Esta librería posee una serie de objetos encargados de capturar y serializar esta información, de manera que pueda ser manejada tal que así:
```php
if (empty($existingAccounts)) {
    // Obtenemos el resultado de la creación de un account.
    $createResult = Sharpspring::createAccount($accountModel); 
    
    // De dicho resultado, obtenemos el ID (del account que acabmos de crear).
    $accountId = $createResult->getId();

    //Y por ejemplo, podemos usarlo para asociar dicho Account a una oportunidad.
    $data = [
        'ownerID' => $agent->id,
        'accountID' => $accountId,
        'probability' => 0
        'dealStageID' => 502798339,
        ...
    ];
    
    $opportunity = new Opportunity($data);

    Sharpspring::createOpportunity($opportunity); 
}
```
