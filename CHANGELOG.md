# Release 1.0.0
## New features
* Added automatic phpDocumentor generator via **gitlab-ci**.
## Changes
* Updated README.md
* Improved client extendability.
* Improved SharpSpring exceptions handling.
## Fixes
* Removed some trivial docblocks. 
