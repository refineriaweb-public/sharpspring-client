<?php
namespace Rw\SharpspringApi\Models;

use Rw\SharpspringApi\Model;

/**
 * Class UserProfile
 *
 * The User Profile table consists of SharpSpring user accounts.
 * This ID can be used in the lead owner field in the Lead table as well as in the Opportunity table.
 *
 * @package Rw\sharpspring-api
 */
class UserProfile extends Model
{
    /**
     * @inheritDoc
     */
    protected $attributes = [
        'id',
        'firstName',
        'lastName',
        'displayName',
        'emailAddress',
        'isActive',
        'isReseller',
        'userTimezone',
        'phone'
    ];
}
