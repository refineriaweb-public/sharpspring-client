<?php
namespace Rw\SharpspringApi\Models;

use Rw\SharpspringApi\Model;

/**
 * Class Field
 *
 * The field table contains metadata for a sharpspring field. This includes both lead and opportunity fields.
 * The 'system name' is a key that can be specified in any POST to create or update an object.
 *
 * @package Rw\sharpspring-api
 */
class Field extends Model
{
    /**
     * @inheritDoc
     */
    protected $attributes = [
        'id',
        'relationship',
        'systemName',
        'label',
        'source',
        'dataType',
        'dataLength',
        'isRequired',
        'isCustom',
        'isActive',
        'isAvailableInContactManager',
        'isEditableInContactManager',
        'isAvailableInForms'
    ];
}
