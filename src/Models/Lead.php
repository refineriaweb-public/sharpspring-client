<?php
namespace Rw\SharpspringApi\Models;

use Rw\SharpspringApi\ModelWithCustom;

/**
 * Class Lead
 *
 * The Lead table consists of prospects who are possibly interested in your product.
 *
 * @package Rw\sharpspring-api
 */
class Lead extends ModelWithCustom
{
    const OPEN = 'open';
    const UNQUALIFIED = 'unqualified';
    const QUALIFIED = 'qualified';
    const CONTACT = 'contact';
    const CUSTOMER = 'customer';

    /**
     * @inheritDoc
     */
    protected $attributes = [
        'id',
        'accountID',
        'ownerID',
        'campaignID',
        'leadStatus',
        'leadScore',
        'leadScoreWeighted',
        'persona',
        'active',
        'firstName',
        'lastName',
        'emailAddress',
        'companyName',
        'title',
        'street',
        'city',
        'country',
        'state',
        'zipcode',
        'website',
        'phoneNumber',
        'trackingID',
        'officePhoneNumber',
        'phoneNumberExtension',
        'mobilePhoneNumber',
        'faxNumber',
        'description',
        'industry',
        'isUnsubscribed',
        'isOptedIn',
        'updateTimestamp',
        'createTimestamp'
    ];
}
