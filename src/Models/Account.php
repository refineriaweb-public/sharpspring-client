<?php
namespace Rw\SharpspringApi\Models;

use Rw\SharpspringApi\ModelWithCustom;

/**
 * Class Account
 *
 * An Account is an organization or person that is associated with an Opportunity.
 *
 * @package Rw\sharpspring-api
 */
class Account extends ModelWithCustom
{
    /**
     * @inheritDoc
     */
    protected $attributes = [
        'id',
        'ownerID',
        'accountName',
        'industry',
        'phone',
        'annualRevenue',
        'numberOfEmployees',
        'website',
        'yearStarted',
        'fax',
        'billingCity',
        'billingCountry',
        'billingPostalCode',
        'billingState',
        'billingStreetAddress',
        'shippingCity',
        'shippingCountry',
        'shippingPostalCode',
        'shippingState',
        'shippingStreetAddress'
    ];
}
