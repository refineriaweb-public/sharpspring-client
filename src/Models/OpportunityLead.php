<?php
namespace Rw\SharpspringApi\Models;

use Rw\SharpspringApi\Model;

/**
 * Class OpportunityLead
 *
 * OpportunityLead is a mapping table that represents an association between an Opportunity and Lead.
 * Each Opportunity in SharpSpring can consist of multiple Leads.
 *
 * @package Rw\sharpspring-api
 */
class OpportunityLead extends Model
{
    /**
     * @inheritDoc
     */
    protected $attributes = [
        'id',
        'opportunityID',
        'leadID'
    ];
}
