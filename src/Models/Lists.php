<?php
namespace Rw\SharpspringApi\Models;

use Rw\SharpspringApi\Model;

/**
 * Class Lists
 *
 * A List is a segmented audience of leads in SharpSpring.
 *
 * @package Rw\sharpspring-api
 */
class Lists extends Model
{
    /**
     * @inheritDoc
     */
    protected $attributes = [
        'id',
        'name',
        'memberCount',
        'removedCount',
        'createTimestamp',
        'description'
    ];
}
