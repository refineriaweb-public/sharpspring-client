<?php
namespace Rw\SharpspringApi\Models;

use Rw\SharpspringApi\Model;

/**
 * Class Email
 *
 * An html email created in SharpSpring.
 *
 * @package Rw\sharpspring-api
 */
class Email extends Model
{
    /**
     * @inheritDoc
     */
    protected $attributes = [
        'id',
        'html',
        'emailName',
        'subject',
        'fromName',
        'fromEmail',
        'replyTo',
        'unsubCategory',
        'contactManager',
        'repeatable',
        'fromLeadOwner'
    ];
}
