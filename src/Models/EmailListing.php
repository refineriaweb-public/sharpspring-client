<?php
namespace Rw\SharpspringApi\Models;

use Rw\SharpspringApi\Model;

/**
 * Class EmailListing
 *
 * An Email Listing contains information about a group of emails, including the id, HTML content, subject line, and thumbnail image.
 *
 * @package Rw\sharpspring-api
 */
class EmailListing extends Model
{
    /**
     * @inheritDoc
     */
    protected $attributes = [
        'id',
        'createTimestamp',
        'title',
        'subject',
        'thumbnail'
    ];
}
