<?php
namespace Rw\SharpspringApi\Models;

use Rw\SharpspringApi\Model;

/**
 * Class DealStage
 *
 * A Deal Stage is a custom state for your Opportunity pipeline.
 *
 * @package Rw\sharpspring-api
 */
class DealStage extends Model
{
    /**
     * @inheritDoc
     */
    protected $attributes = [
        'id',
        'dealStageName',
        'description',
        'defaultProbability',
        'weight',
        'isEditable'
    ];
}
