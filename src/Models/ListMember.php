<?php
namespace Rw\SharpspringApi\Models;

use Rw\SharpspringApi\Model;

/**
 * Class ListMember
 *
 * ListMember is a mapping table that corresponds leads in SharpSpring to the list they are members of.
 *
 * @package Rw\sharpspring-api
 */
class ListMember extends Model
{
    protected $attributes = [
        'id',
        'listID',
        'memberID',
        'leadID',
        'isRemoved'
    ];
}
