<?php
namespace Rw\SharpspringApi\Models;

use Rw\SharpspringApi\Model;

/**
 * Class Client
 *
 * The Client table contains information on Agency instances managed by the Partner instance.
 *
 * @package Rw\sharpspring-api
 */
class Client extends Model
{
    /**
     * @inheritDoc
     */
    protected $attributes = [
        'id',
        'companyName',
        'streetAddress',
        'zipCode',
        'country',
        'state',
        'city'
    ];
}
