<?php
namespace Rw\SharpspringApi\Models;

use Rw\SharpspringApi\Model;

/**
 * Class ListTag
 *
 * ListTag is a table of all list tags in an instance of SharpSpring.
 * List tags are used to group similar lists together for aggregate list sends, automation, and reporting.
 *
 * @package Rw\sharpspring-api
 */
class ListTag extends Model
{
    /**
     * @inheritDoc
     */
    protected $attributes = [
        'id',
        'label',
        'objectType'
    ];
}
