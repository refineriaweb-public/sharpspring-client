<?php
namespace Rw\SharpspringApi\Models;

use Rw\SharpspringApi\Model;

/**
 * Class Campaign
 *
 * A Campaign is a marketing project that we track in SharpSpring.
 *
 * @package Rw\sharpspring-api
 */
class Campaign extends Model
{
    /**
     * @inheritDoc
     */
    protected $attributes = [
        'id',
        'campaignName',
        'campaignType',
        'campaignAlias',
        'campaignOrigin',
        'qty',
        'price',
        'goal',
        'otherCosts',
        'startDate',
        'endDate',
        'isActive'
    ];
}
