<?php
namespace Rw\SharpspringApi\Models;

use Rw\SharpspringApi\ModelWithCustom;

/**
 * Class Opportunity
 *
 * An Opportunity represents a potential deal that has an expected value.
 * An Opportunity can be associated with a Lead or Account, as well as a Campaign.
 *
 * @package Rw\sharpspring-api
 */
class Opportunity extends ModelWithCustom
{
    /**
     * @inheritDoc
     */
    protected $attributes = [
        'id',
        'ownerID',
        'dealStageID',
        'accountID',
        'campaignID',
        'opportunityName',
        'probability',
        'amount',
        'isClosed',
        'isWon',
        'isActive',
        'closeDate',
        'originatingLeadID',
        'primaryLeadID'
    ];

    /**
     * @inheritDoc
     */
    protected $nullable = [
        'opportunityName',
        'closeDate'
    ];
}
