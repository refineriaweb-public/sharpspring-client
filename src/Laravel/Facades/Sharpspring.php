<?php
namespace Rw\SharpspringApi\Laravel\Facades;

use Illuminate\Support\Facades\Facade;
use Rw\SharpspringApi\SharpspringClient;

/**
 * Class Sharpspring
 *
 * @package dllobell\sharpspring
 */
class Sharpspring extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return SharpspringClient::class;
    }
}
