<?php
namespace Rw\SharpspringApi\Laravel;

use Illuminate\Support\ServiceProvider;
use Rw\SharpspringApi\SharpspringClient;

/**
 * Class SharpspringServiceProvider
 *
 * @package Rw\sharpspring
 */
class SharpspringServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/config/sharpspring.php' => config_path('sharpspring.php'),
        ], 'config');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SharpspringClient::class, function ($app) {
            return new SharpspringClient(
                $app['config']->get('sharpspring.account_id'),
                $app['config']->get('sharpspring.secret_key')
            );
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [SharpspringClient::class];
    }
}
