<?php
return [
    'account_id' => env('SHARPSPRING_ACCOUNT_ID', ''),
    'secret_key' => env('SHARPSPRING_SECRET_KEY', '')
];
