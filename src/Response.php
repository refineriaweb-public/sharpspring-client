<?php
namespace Rw\SharpspringApi;

use Exception;
use Illuminate\Support\Facades\Log;
use Rw\SharpspringApi\Exceptions\ResponseException;

/**
 * Class Response
 *
 * Class for Sharpspring responses
 *
 * @package Rw\sharpspring-api
 */
class Response
{
    protected $request;

    protected $body;

    protected $exception;

    /**
     * Create a new Sharpspring response instance.
     *
     * @return void
     */
    public function __construct(Request $request, $rawResponse)
    {
        $this->request = $request;

        $this->decodeResponse($rawResponse);
    }

    public function getRequest()
    {
        return $this->request;
    }

    public function getEndpoint()
    {
        return $this->getRequest()->getEndpoint();
    }

    public function getBody()
    {
        return $this->body;
    }

    public function getResult()
    {
        try {
            return $this->getBody()->result;
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return null;
        }
    }

    public function getError()
    {
        if (!is_null($this->getBody()) && (isset($this->getBody()->error) && !empty($this->getBody()->error))) {
            return $this->getBody()->error;
        }

        return null;
    }

    public function getId()
    {
        return $this->getBody()->id;
    }

    public function getCallCount()
    {
        return $this->getBody()->callCount;
    }

    public function getQueryLimit()
    {
        return $this->getBody()->queryLimit;
    }

    public function getException()
    {
        return $this->exception;
    }

    public function isError()
    {
        return !is_null($this->getError());
    }

    public function makeException()
    {
        return $this->exception = new ResponseException($this);
    }

    protected function decodeResponse($rawResponse)
    {
        $this->body = json_decode($rawResponse);
    }
}
