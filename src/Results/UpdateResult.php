<?php
namespace Rw\SharpspringApi\Results;

use Rw\SharpspringApi\Result;

/**
 * Class UpdateResult
 *
 * UpdateResult contains information about the success of an update operation, and an array of any errors associated with the operation.
 *
 * @package Rw\sharpspring-api
 */
class UpdateResult extends Result
{
}
