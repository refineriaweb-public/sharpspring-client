<?php
namespace Rw\SharpspringApi\Results;

use Rw\SharpspringApi\Result;

/**
 * Class CreateResult
 *
 * CreateResult contains information about the success of a create operation, an array of any errors associated with the operation, and the ID of the newly created record.
 *
 * @package Rw\sharpspring-api
 */
class CreateResult extends Result
{
    protected $id;

    /**
     * Create a new CreateResult instance.
     *
     * @return void
     */
    public function __construct(array $attributes)
    {
        parent::__construct($attributes);

        if (array_key_exists('id', $attributes)) {
            $this->id = $attributes['id'];
        }
    }

    public function getId()
    {
        return $this->id;
    }
}
