<?php
namespace Rw\SharpspringApi\Results;

use Rw\SharpspringApi\Result;

/**
 * Class DeleteResult
 *
 * DeleteResult contains information about the success of a delete operation, and an array of any errors associated with the operation.
 *
 * @package Rw\sharpspring-api
 */
class DeleteResult extends Result
{
}
