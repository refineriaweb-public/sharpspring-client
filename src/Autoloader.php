<?php
namespace Rw\SharpspringApi;

/**
 * Class Autoloader
 *
 * If you are not using Composer to manage class autoloading, here's an autoloader for this package.
 *
 * @package Rw\sharpspring-api
 */
class Autoloader
{
    /**
     * Registers Rw\SharpspringApi\Autoloader as an SPL autoloader.
     *
     * @param  bool $prepend Whether to prepend the autoloader or not.
     *
     * @return void
     */
    public static function register($prepend = false)
    {
        spl_autoload_register(array(__CLASS__, 'autoload'), true, $prepend);
    }

    /**
     * Handles autoloading of classes.
     *
     * @param  string $class A class name.
     *
     * @return void
     *
     * @throws Exception
     */
    public static function autoload($class)
    {
        if (strpos($class, 'Rw\\SharpspringApi\\') !== 0) {
            return;
        }

        if (is_file($file = __DIR__ . str_replace(array('Rw\\SharpspringApi\\', '\\'), '/', $class).'.php')) {
            require $file;
        }
    }
}
