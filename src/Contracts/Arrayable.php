<?php
namespace Rw\SharpspringApi\Contracts;

/**
 * Interface Arrayable
 *
 * @package Rw\sharpspring-api
 */
interface Arrayable
{
    /**
     * Convert the model instance to an array.
     *
     * @return array
     */
    public function toArray();
}
