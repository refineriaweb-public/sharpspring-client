<?php
namespace Rw\SharpspringApi\Contracts;

/**
 * Interface Jsonable
 *
 * @package Rw\sharpspring-api
 */
interface Jsonable
{
    /**
     * Convert the object to its JSON representation.
     *
     * @param  int  $options
     * @return string
     */
    public function toJson($options = 0);
}
