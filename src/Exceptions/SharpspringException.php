<?php
namespace Rw\SharpspringApi\Exceptions;

use Exception;

/**
 * Class SharpspringException
 *
 * @package Rw\sharpspring-api
 */
class SharpspringException extends Exception
{
}
