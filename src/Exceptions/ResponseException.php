<?php
namespace Rw\SharpspringApi\Exceptions;

use Rw\SharpspringApi\Response;
use Rw\SharpspringApi\Exceptions\SharpspringException;

/**
 * Class SharpspringException
 *
 * @package Rw\sharpspring-api
 */
class ResponseException extends SharpspringException
{
    protected $response;

    protected $data;

    public function __construct(Response $response, SharpspringException $previous = null)
    {
        $this->response = $response;

        $error = $response->getError();
        if (is_array($response->getError())) {
            $error = $response->getError()[0];
        }

        $this->data = $error->data;

        if (str_contains($error->message, 'Invalid parameters')) {
            dd($this->data);
        }

        parent::__construct($error->message, $error->code, $previous);
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function getData()
    {
        return $this->data;
    }

    public function __get($key)
    {
        if (isset($this->data->{$key})) {
            return $this->data->{$key};
        }

        return null;
    }
}
